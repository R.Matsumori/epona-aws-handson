# バックエンドパイプラインの構築

バックエンドのパイプラインを整えるためには `cd_pipeline_backend` patternを使っていきます[^1]。

[^1]: GitLabのロゴは[GitLab](https://about.gitlab.com/)によって制作されたものであり、[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)の下に提供されています。

![cd_pipeline_backend](../resources/cd_pipeline_backend.jpg)

## アジェンダ

`cd_pipeline_backend_trigger` は講師の指示のもと、コードを修正していただきました。  
`cd_pipeline_backend` ではドキュメントの指示に従い、受講者の皆さん自身で既存コードを修正し適用していきます。

何のための作業なのか、復習する機会として活用してください。

1. [cd_pipeline_backend patternについて](#cd_pipeline_backend-patternについて)
   1. [依存しているパターン](#依存しているパターン)
   1. [適用後の構成図](#適用後の構成図)
1. [事前準備](#事前準備)
1. [パイプライン構築](#パイプライン構築)
   1. [backend](#backend)
      1. `cd_pipeline_backend pattern` 作成
      1. `cd_pipeline_backend pattern` 適用
      1. `cd_pipeline_backend_trigger pattern` との連携
      1. 動作確認
   1. [notifier](#notifier)
      1. `cd_pipeline_backend pattern` 作成
      1. `cd_pipeline_backend pattern` 適用
      1. `cd_pipeline_backend_trigger pattern` との連携
      1. 動作確認

## cd_pipeline_backend patternについて

`cd_pipeline_backend` patternは、バックエンドアプリケーションのデプロイを行うパイプラインを構築するものです。

[cd_pipeline_backend pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend/)

Eponaのドキュメントを用いて、以下について解説していきます。

* 概要
* 想定する適用対象環境
* 前提事項
* 環境ごとのデプロイ戦略
  * デプロイを開始するトリガ
  * また、デプロイを承認不要/必須に変更可

### 依存しているパターン

`cd_pipeline_backend pattern` は以下patternに依存しています。

|依存パターン名|依存しているリソース|
|:---|:---|
|`ci_pipeline pattern`|コンテナイメージを格納したコンテナレジストリ|
|`cd_pipeline_backend_trigger pattern`|Delivery環境のリソース取得のためのルールやロール|
|`public_traffic_container_service pattern`|デプロイ対象となるECS|

[依存するpattern](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend/#%E4%BE%9D%E5%AD%98%E3%81%99%E3%82%8Bpattern)

上記依存しているpatternの適用は、皆さん自身の手で対応済みです[^1]。

![cd_pipeline_backend patternとの連携 pattern 依存関係](../resources/cd_pipeline_backend_dependencies.jpg)

### 適用後の構成図

上記環境から `cd_pipeline_backend pattern` を適用した場合の構成図です[^1]。

![cd_pipeline_backend pattern 適用後構成](../resources/cd_pipeline_backend_after_apply.jpg)

---

## 事前準備

### 実行ユーザーの設定

今回はRuntime環境へ適用することになります。
お好みのコマンドラインツールを開いて、以下サイトを参考にRuntimeの実行ユーザーへ切り替えてください。

[AWS CLI を設定する環境変数](https://docs.aws.amazon.com/ja_jp/cli/latest/userguide/cli-configure-envvars.html)

上記設定が無事成功しているかを確認していきます。  
`$ aws sts get-caller-identity --query Arn --output text` を実行してください。

以下のように出力されたら成功です。  
※ `[LastName]` は事前に指定されたユーザー名の値が出力されていること。

```shell script
$ aws sts get-caller-identity --query Arn --output text
arn:aws:iam::938285887320:user/[LastName]StagingTerraformer
```

## パイプライン構築

Runtime環境へデプロイするために、イベント発火後にAmazon ECSへデプロイするパイプラインを構築します。

### ゴール

* Terraformでデプロイ実行のトリガとなるイベントを受け、パイプラインを起動するようなルールを構築する
* Terraformでイベント発火後のパイプライン（AWS CodePipeline）を構築する
* TerraformでCodePipelineが利用するArtifact Store(Amazon S3)を暗号化するための鍵を構築します
* 上記をAWSコンソールやAPIを叩くことで確認します

### 進め方

1. Eponaのガイドにあるサンプルコードをコピーする
1. 利用するアプリケーションに合わせて、サンプルコードを修正する
1. その他、Terraformのバージョン設定など必要なファイルを作成する
1. 適用する
1. `cd_pipeline_backend_trigger pattern` との連携をする
1. パイプラインを動かすために、Amazon S3へ設定ファイルを配置する
1. パイプラインが成功していることをAWSコンソールから確認する

---

### backend

REST APIのアプリケーションであるbackendから作成していきましょう。

#### cd_pipeline_backend pattern 作成

cd_pipeline_backendパターンのbackend向けディレクトリに移動します。  
※[last_name] については、事前に指定されたユーザー名のディレクトリを利用すること。

```shell script
$ cd epona-aws-handson/[last_name]/runtimes/staging/cd_pipeline_backend_backend
```

##### テンプレートファイルの作成

最初に、テンプレートである `main.tf` ファイルを新規作成します。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch main.tf
```

では、次にEponaのガイドに記載があるサンプルコードをコピーし、貼り付けてください。

[サンプルコード](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend/#%E3%82%B5%E3%83%B3%E3%83%97%E3%83%AB%E3%82%B3%E3%83%BC%E3%83%89)

以下のようにコードを書き替えてください。

* `[last_name]` , `[LastName]` は適宜修正
* `[last_name]` 以外にも変更箇所があります。よく確認してください
  * AWS側の文字数制限によるもの
  * `handson-chat-application` のアーキテクチャによるもの（backend, notifierに分かれている等）

なお、コメントにて変更点の解説をしていますが、コメントまで記載する必要はありません。

```terraform
# TODO:AWSプロバイダーを使うための設定を修正
provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/[LastName]TerraformExecutionRole"
  }
}

# TODO:module名を"cd_pipeline_backend_backend"に変更
module "cd_pipeline_backend_backend" {
  # TODO: 参照するEponaモジュールのバージョンを記載する
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend?ref=v0.2.1"

  # TODO: DeliveryアカウントIDに変更
  delivery_account_id = "938285887320"
  # TODO: backend向けECRリポジトリ名に変更。
  # TODO: タグをlatestに変更。また、チャットアプリケーションであるためアーティファクト名を chat に変更
  ecr_repositories = {
    [last_name]-chat-example-backend = {
      tag            = "latest" # リリース対象タグ
      artifact_name  = "chat"
      container_name = "IMAGE1_NAME"
    }
  }

  # Delivery環境のECRにCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_backend_triggerの output として出力される
  # TODO: cd_pipeline_backend_trigger_backend の output として出力されたARNに変更
  cross_account_codepipeline_access_role_arn = "arn:aws:iam::938285887320:role/[LastName]-Pipeline-BackAccessRole"
  # TODO: cd_pipeline_backend_trigger_backend の bucket_name と同じ名前を指定する
  source_bucket_name = "[last_name]-pipeline-back-source"

  # TODO: backend向けのデータを参照するため、 staging_public_traffic_container_service_backend に変更
  cluster_name = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service_backend.ecs_cluster_name
  service_name = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service_backend.ecs_service_name

  # TODO: pipeline_name, artifact_store_bucket_name をわかりやすいリソース名となるように変更
  pipeline_name                       = "[last_name]-pipeline-back"
  artifact_store_bucket_name          = "[last_name]-back-artfct"
  artifact_store_bucket_force_destroy = true   # TODO: ハンズオンではS3バケットの削除保護を無効化するため、trueに変更

  # TODO: backend向けのデータを参照するため、 staging_public_traffic_container_service_backend に変更
  prod_listener_arns = [data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service_backend.load_balancer_prod_listener_arn]
  target_group_names = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service_backend.load_balancer_target_group_names

  # ECSへのデプロイに使用するデプロイメントグループ名やCodeDeployのアプリケーション名をわかりやすいリソース名となるように変更
  codedeploy_app_name         = "[last_name]-chat-backend"
  deployment_group_name_ecs   = "[last_name]-chat-backend-group"

  # TODO: ハンズオンではデプロイを承認不要とする
  deployment_require_approval = false
}
```

##### 外部データを参照するためのファイルを作成

`instance_dependencies.tf` ファイルを新規作成し、外部データを参照できるようにしましょう。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch instance_dependencies.tf
```

`instance_dependencies.tf` には、以下のように記載してください。

```terraform
data "terraform_remote_state" "staging_public_traffic_container_service_backend" {
  backend = "s3"

  config = {
    bucket         = "[last_name]-staging-terraform-tfstate"
    key            = "public_traffic_container_service_backend/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "[last_name]_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/[LastName]StagingTerraformBackendAccessRole"
  }
}
```

##### Terraformバージョンを指定するためのファイルを作成

では次にTerraformのバージョンを指定するために、 `versions.tf` を新規作成していきましょう。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch versions.tf
```

`versions.tf` には、以下のように記載してください。

```terraform
terraform {
  required_version = "0.13.5"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.18.0"
    }
  }

  backend "s3" {
    bucket         = "[last_name]-staging-terraform-tfstate"
    key            = "cd_pipeline_backend_backend/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "[last_name]_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/[LastName]StagingTerraformBackendAccessRole"
  }
}
```

##### モジュールの出力を行うためのファイルを作成

次は、実行後の戻り値を出力するために `outputs.tf` を新規作成していきます。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch outputs.tf
```

`outputs.tf` には、以下のように記載してください。

```terraform
output "cd_pipeline_backend_backend" {
  value = module.cd_pipeline_backend_backend
}
```

#### cd_pipeline_backend pattern 適用

##### ワークスペースの初期化

既に移動していただいていますが、以下ディレクトリに移動していることを確認してください。

```shell script
$ cd epona-aws-handson/[last_name]/runtimes/staging/cd_pipeline_backend_backend
```

Terraformのワークスペースを初期化します。

```shell script
$ terraform init
```

以下のような表示が出ればOKです。

```shell script
$ terraform init
…
Terraform has been successfully initialized!
…
```

##### terraform apply

変更を反映させるため、`terraform apply` します。

以下コマンドを入力してください。

```shell script
$ terraform apply
```

作成されるリソース（実行計画）を確認してください。

* ファイルの変更が反映されていること
* `Plan: 20 to add, 0 to change, 0 to destroy` という出力があること

```shell script
$ terraform apply
…Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value:
```

問題なければ `yes` と入力し、Enterを押下してください。

* `Apply complete! Resources: 20 added, 0 changed, 0 destroyed.` という出力があること
* `Outputs` に `cd_pipeline_backend_backend` についての出力がされていること

```shell script
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value: yes

…
Apply complete! Resources: 20 added, 0 changed, 0 destroyed.
Outputs:
cd_pipeline_backend_backend = {
  "artifact_store_bucket_arn" = "arn:aws:s3:::[last_name]-chat-backend-artfct"
…
}
```

#### cd_pipeline_backend_trigger patternとの連携

`cd_pipeline_backend_trigger` を再適用する必要があります。

[patternの適用順序について](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend_trigger/#pattern%E3%81%AE%E9%81%A9%E7%94%A8%E9%A0%86%E5%BA%8F)

<!-- textlint-disable ja-technical-writing/ja-no-mixed-period -->

1.Delivery環境に対するcd_pipeline_backend_trigger patternの適用
> 2.Runtime環境に対するcd_pipeline_backend patternの適用  
3.2.で作成したリソースのARNをパラメータに設定し、Delivery環境へcd_pipeline_backend_trigger patternを再適用

<!-- textlint-enable ja-technical-writing/ja-no-mixed-period -->

再適用については、以下手順に従ってください。

[cd_pipeline_backend patternとの連携](./cd_pipeline_backend_trigger.md#cd_pipeline_backend-patternとの連携)

---

### notifier

WebSocketで通知するアプリケーションであるnotifierの作成手順です。

notifierについては、時間の都合上講師により実施しています。  
必要があれば研修後に参照してください。

<!-- markdownlint-disable no-inline-html -->
<details>
  <summary>notifier 構築手順</summary>
<!-- markdownlint-enable no-inline-html -->

#### cd_pipeline_backend pattern 作成

cd_pipeline_backendパターンのnotifier向けディレクトリに移動します。  
※[last_name] については、事前に指定されたユーザー名のディレクトリを利用すること。

```shell script
$ cd epona-aws-handson/[last_name]/runtimes/staging/cd_pipeline_backend_notifier
```

##### テンプレートファイルの作成

最初に、テンプレートである `main.tf` ファイルを新規作成します。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch main.tf
```

では、次にEponaのガイドに記載があるサンプルコードをコピーし、貼り付けてください。

[サンプルコード](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend/#%E3%82%B5%E3%83%B3%E3%83%97%E3%83%AB%E3%82%B3%E3%83%BC%E3%83%89)

以下のようにコードを書き替えてください。 `[last_name]` , `[LastName]` は適宜修正してください。  
なお、コメントにて変更点の解説をしていますが、コメントまで記載する必要はありません。

```terraform
# TODO:AWSプロバイダーを使うための設定を修正
provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/[LastName]TerraformExecutionRole"
  }
}

# TODO:module名を"cd_pipeline_backend_notifier"に変更
module "cd_pipeline_backend_notifier" {
  # TODO: 参照するEponaモジュールのバージョンを記載する
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend?ref=v0.2.1"

  # TODO: DeliveryアカウントIDに変更
  delivery_account_id = "938285887320"
  # TODO: notifier向けECRリポジトリ名に変更。
  # TODO: タグをlatestに変更。また、notifierアプリケーションであるためアーティファクト名を ntfr に変更
  ecr_repositories = {
    [last_name]-chat-example-ntfr = {
      tag            = "latest" # リリース対象タグ
      artifact_name  = "ntfr"
      container_name = "IMAGE1_NAME"
    }
  }

  # Delivery環境のECRにCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_backend_triggerの output として出力される
  # TODO: cd_pipeline_backend_trigger_notifier の output として出力されたARNに変更
  cross_account_codepipeline_access_role_arn = "arn:aws:iam::938285887320:role/[LastName]-Pipeline-NtfrAccessRole"
  # TODO: cd_pipeline_backend_trigger_notifier の bucket_name と同じ名前を指定する
  source_bucket_name = "[last_name]-pipeline-ntfr-source"

  # TODO: notifier向けのデータを参照するため、 staging_public_traffic_container_service_notifier に変更
  cluster_name = data.terraform_remote_state.staging_public_traffic_container_service_notifier.outputs.public_traffic_container_service_notifier.ecs_cluster_name
  service_name = data.terraform_remote_state.staging_public_traffic_container_service_notifier.outputs.public_traffic_container_service_notifier.ecs_service_name

  # TODO: pipeline_name, artifact_store_bucket_name をわかりやすいリソース名となるように変更
  pipeline_name              = "[last_name]-pipeline-ntfr"
  artifact_store_bucket_name = "[last_name]-ntfr-artfct"

  # TODO: notifier向けのデータを参照するため、 staging_public_traffic_container_service_notifier に変更
  prod_listener_arns = [data.terraform_remote_state.staging_public_traffic_container_service_notifier.outputs.public_traffic_container_service_notifier.load_balancer_prod_listener_arn]
  target_group_names = data.terraform_remote_state.staging_public_traffic_container_service_notifier.outputs.public_traffic_container_service_notifier.load_balancer_target_group_names

  # ECSへのデプロイに使用するデプロイメントグループ名やCodeDeployのアプリケーション名をわかりやすいリソース名となるように変更
  codedeploy_app_name         = "[last_name]-chat-ntfr"
  deployment_group_name_ecs   = "[last_name]-chat-ntfr-group"

  # TODO: ハンズオンではデプロイを承認不要とする
  deployment_require_approval = false
}
```

##### 外部データを参照するためのファイルを作成

`instance_dependencies.tf` ファイルを新規作成し、外部データを参照できるようにしましょう。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch instance_dependencies.tf
```

`instance_dependencies.tf` には、以下のように記載してください。

```terraform
data "terraform_remote_state" "staging_public_traffic_container_service_notifier" {
  backend = "s3"

  config = {
    bucket         = "[last_name]-staging-terraform-tfstate"
    key            = "public_traffic_container_service_notifier/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "[last_name]_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/[LastName]StagingTerraformBackendAccessRole"
  }
}
```

##### Terraformバージョンを指定するためのファイルを作成

では次に `versions.tf` を新規作成していきましょう。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch versions.tf
```

`versions.tf` には、以下のように記載してください。

```terraform
terraform {
  required_version = "0.13.5"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.18.0"
    }
  }

  backend "s3" {
    bucket         = "[last_name]-staging-terraform-tfstate"
    key            = "cd_pipeline_backend_notifier/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "[last_name]_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/[LastName]StagingTerraformBackendAccessRole"
  }
}
```

##### モジュールの出力を行うためのファイルを作成

次は、実行後の戻り値を出力するために `outputs.tf` を新規作成していきます。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch outputs.tf
```

`outputs.tf` には、以下のように記載してください。

```terraform
output "cd_pipeline_backend_notifier" {
  value = module.cd_pipeline_backend_notifier
}
```

#### cd_pipeline_backend pattern 適用

##### ワークスペースの初期化

既に移動していただいていますが、以下ディレクトリに移動していることを確認してください。

```shell script
$ cd epona-aws-handson/[last_name]/runtimes/staging/cd_pipeline_backend_notifier
```

Terraformのワークスペースを初期化します。

```shell script
$ terraform init
```

以下のような表示が出ればOKです。

```shell script
$ terraform init
…
Terraform has been successfully initialized!
…
```

##### terraform apply

変更を反映させるため、`terraform apply` します。

以下コマンドを入力してください。

```shell script
$ terraform apply
```

作成されるリソース（実行計画）を確認してください。

* ファイルの変更が反映されていること
* `Plan: 20 to add, 0 to change, 0 to destroy` という出力があること

```shell script
$ terraform apply
…Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value:
```

問題なければ `yes` と入力し、Enterを押下してください。

* `Apply complete! Resources: 20 added, 0 changed, 0 destroyed.` という出力があること
* `Outputs` に `cd_pipeline_backend_notifier` についての出力がされていること

```shell script
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value: yes

…
Apply complete! Resources: 20 added, 0 changed, 0 destroyed.
Outputs:
cd_pipeline_backend_notifier = {
  "artifact_store_bucket_arn" = "arn:aws:s3:::[last_name]-chat-ntfr-artfct"
…
}
```

#### cd_pipeline_backend_trigger patternとの連携

`cd_pipeline_backend_trigger` を再適用する必要があります。

[patternの適用順序について](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend_trigger/#pattern%E3%81%AE%E9%81%A9%E7%94%A8%E9%A0%86%E5%BA%8F)

<!-- textlint-disable ja-technical-writing/ja-no-mixed-period -->

1.Delivery環境に対するcd_pipeline_backend_trigger patternの適用
> 2.Runtime環境に対するcd_pipeline_backend patternの適用  
3.2.で作成したリソースのARNをパラメータに設定し、Delivery環境へcd_pipeline_backend_trigger patternを再適用

<!-- textlint-enable ja-technical-writing/ja-no-mixed-period -->

再適用については、以下手順に従ってください。

[cd_pipeline_backend patternとの連携](./cd_pipeline_backend_trigger.md#cd_pipeline_backend-patternとの連携)

<!-- markdownlint-disable no-inline-html -->
</details>
<!-- markdownlint-enable no-inline-html -->
